package com.zvz.sys.service.impl;

import com.zvz.sys.dao.UserMapper;
import com.zvz.sys.entity.User;
import com.zvz.sys.service.IUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by zvz on 2017/5/7.
 */
@Service("userService")
public class UserService implements IUserService {
    @Resource
    private UserMapper userDao;

    public User getUserById(Long userId) {
        return this.userDao.selectByPrimaryKey(userId);
    }

    @Override
    public User findByUsername(String userName) {
        return this.userDao.selectByUserName(userName);
    }

    @Override
    public List<String> queryAllPerms(Long userId) {
        return this.userDao.queryAllPerms(userId);
    }

}
