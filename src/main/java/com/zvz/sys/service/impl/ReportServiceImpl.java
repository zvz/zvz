package com.zvz.sys.service.impl;

import com.zvz.sys.dao.ReportMapper;
import com.zvz.sys.entity.Report;
import com.zvz.sys.service.IReportService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zvz on 2017/5/9.
 */
@Service("reportServiceImpl")
public class ReportServiceImpl implements IReportService {

    @Resource
    private ReportMapper reportDao;

    @Override
    public void save(Report report) {
        reportDao.insert(report);
    }
}
