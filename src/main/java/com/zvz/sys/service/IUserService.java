package com.zvz.sys.service;

import com.zvz.sys.entity.User;

import java.util.List;

/**
 * Created by zvz on 2017/5/7.
 */
public interface IUserService {

     User getUserById(Long userId);

     User findByUsername(String userName);

     List<String> queryAllPerms(Long userId);
}
