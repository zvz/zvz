package com.zvz.sys.service;

import com.zvz.sys.entity.Report;

/**
 * Created by zvz on 2017/5/9.
 */
public interface IReportService {

    void save(Report report);
}
