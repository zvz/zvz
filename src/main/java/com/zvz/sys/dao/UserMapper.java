package com.zvz.sys.dao;

import com.zvz.sys.entity.User;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Long userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Long userId);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<String> queryAllPerms(Long userId);

    User selectByUserName(String userName);
}