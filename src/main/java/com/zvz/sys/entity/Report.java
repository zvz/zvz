package com.zvz.sys.entity;

public class Report {
    private Integer id;

    private String reportName;

    private String sqlStr;

    private String columnName;

    private String headName;

    public Report(Integer id, String reportName, String sqlStr, String columnName, String headName) {
        this.id = id;
        this.reportName = reportName;
        this.sqlStr = sqlStr;
        this.columnName = columnName;
        this.headName = headName;
    }

    public Report() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName == null ? null : reportName.trim();
    }

    public String getSqlStr() {
        return sqlStr;
    }

    public void setSqlStr(String sqlStr) {
        this.sqlStr = sqlStr == null ? null : sqlStr.trim();
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName == null ? null : columnName.trim();
    }

    public String getHeadName() {
        return headName;
    }

    public void setHeadName(String headName) {
        this.headName = headName == null ? null : headName.trim();
    }
}