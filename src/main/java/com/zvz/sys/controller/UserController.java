package com.zvz.sys.controller;

import com.zvz.sys.entity.User;
import com.zvz.sys.service.IUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

@Controller
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserService userService;

    @RequestMapping(value="/showUser/{id}",method= RequestMethod.GET)
    public String toIndex(@PathVariable("id") Long id, Model model) {
        User user = this.userService.getUserById(id);
        model.addAttribute("user", user);
        return "User.jsp";
    }
}