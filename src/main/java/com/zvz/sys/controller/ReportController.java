package com.zvz.sys.controller;

import com.zvz.sys.entity.Report;
import com.zvz.sys.service.IReportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by zvz on 2017/5/9.
 */
@Controller
@RequestMapping("/sys/report")
public class ReportController {

    @Resource
    private IReportService reportServiceImpl;

    @ResponseBody
    @RequestMapping("/save")
    public String save(@RequestBody Report report){
        reportServiceImpl.save(report);
        return "0";
    }
}
